class Solution:
    def exist(self, board: List[List[str]], word: str) -> bool:
        directions = [(0,1), (1,0), (-1,0), (0,-1)]
        r = len(board)
        c = len(board[0])
        
        def valid(row, col): 
            return row < r and row >= 0 and col < c and col >= 0

        def backtrack(row, col, squares, i):
            if i == len(word):
                return True

            for x,y in directions:
                next_row = row+y
                next_col = col+x
                if valid(next_row,next_col) and (next_row,next_col) not in squares:
                    if board[next_row][next_col]== word[i]:
                        squares.add((next_row,next_col))
                        if backtrack(next_row,next_col, squares, i+1):
                            return True
                        squares.remove((next_row,next_col))
            return False

        for i in range(len(board)):
            for j in range(len(board[i])):
                if board[i][j] == word[0] and backtrack(i,j,{(i,j)}, 1):
                    return True
        return False
                
            