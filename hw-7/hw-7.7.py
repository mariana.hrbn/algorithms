from collections import deque

class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
        self.parent = None

class Solution:
    def distanceK(self, root: TreeNode, target: TreeNode, k: int) -> List[int]:

        res = []
        seen = set()

        def add_parent(node, parent):
            if node:
                node.parent = parent
                add_parent(node.left, node)
                add_parent(node.right, node)
        
        def search(node, count):
            if node == None:
                return
            if count == k:
                if node.val not in seen:
                    res.append(node.val)
                return
            else:
                seen.add(node.val)
                search(node.left, count+1)
                search(node.right, count+1)
                if node.parent and node.parent.val not in seen:
                    search(node.parent, count+1)

        add_parent(root, None)
        search(target, 0)

        return res
