class Solution:
    def numIslands(self, grid: List[List[str]]) -> int:
        directions = [(0,1), (1,0), (-1,0), (0,-1)]
        m = len(grid)
        n = len(grid[0])
        seen = set()
        res = 0

        def checkIsland(r,c,m,n):
            return 0 <=r<m and 0<=c<n and grid[r][c] == "1"

        def dfs(r,c):
            for dx,dy in directions:
                next_r =r+ dy
                next_c =c+ dx

                if checkIsland(next_r,next_c,m,n) and (next_r,next_c) not in seen:
                    seen.add((next_r,next_c))
                    dfs(next_r,next_c)

        for r in range(m):
            for c in range(n):
                if checkIsland(r,c,m,n) and (r,c) not in seen:
                    seen.add((r,c))
                    res+=1
                    dfs(r,c)
        return res