"""
# Definition for a Node.
class Node(object):
    def __init__(self, val=None, children=[]):
        self.val = val
        self.children = children
"""

from collections import deque

class Codec:
    def serialize(self, root: 'Node') -> str:
        if not root:
            return ''
        
        def dfs(node):
            if node:
                data = []
                for i in node.children:
                    data.append([i.val, dfs(i)])
                return data

        children = dfs(root)
        return [root.val, children]
        
	
    def deserialize(self, data: str) -> 'Node':
        if not data:
            return None
                
        def dfs(string):
            if not string:
                return

            root = string[0]

            for string_node in string[1:]:
                node = Node(root)
                children = []
                for child in string_node:
                    children.append(dfs(child))
                node.children = children
                return node
                    
        return dfs(data)
        

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))