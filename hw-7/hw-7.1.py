# """
# This is HtmlParser's API interface.
# You should not implement it, or speculate about its implementation
# """
# class HtmlParser(object):
#    def getUrls(self, url):
#        """
#        :type url: str
#        :rtype List[str]
#        """

class Solution:
    def crawl(self, startUrl: str, htmlParser: 'HtmlParser') -> List[str]:

        seen = set()
        hostname = startUrl.split('/')[2]

        def dfs(url, seen):
            seen.add(url)
            urls = htmlParser.getUrls(url)
            for item in urls:
                if item not in seen:
                    if item.split('/')[2] == hostname:
                        dfs(item, seen)
            
        dfs(startUrl, seen)
        return seen