# cook your dish here
def solution(expr:str):
    stack_operands = []
    stack_operators = []
    
    for i in expr:
        if i == ')':
            b = stack_operands.pop()
            op = stack_operators.pop()
            a = stack_operands.pop()
            stack_operands.append(a+b+op)
        elif i.isalpha():
            stack_operands.append(i)
        elif i in ['*', '+', '^', '/', '-']:
            stack_operators.append(i)
    res = ''.join(stack_operands)
    return res
        
if __name__ == "__main__":
    t = int(input())
    
    for _ in range(t):
        expression = input()
        print(solution(expression))


