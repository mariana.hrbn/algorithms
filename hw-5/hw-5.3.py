class Node:
    def __init__(self, val):
        self.val = val
        self.next = None
        self.prev = None


class MyCircularDeque:

    def __init__(self, k: int):
        self.max_size = k
        self.front = None
        self.last = None
        self.size = 0
        

    def insertFront(self, value: int) -> bool:
        if self.isFull():
            return False
        node = Node(value)
        if self.isEmpty():
            self.front = node
            self.last = node
        else:
            tmp = self.front
            self.front = node
            tmp.prev = self.front
            self.front.next = tmp
        self.size +=1
        return True
        

    def insertLast(self, value: int) -> bool:
        if self.isFull():
            return False
        node = Node(value)
        if self.isEmpty():
            self.front = node
            self.last = node
        else:
            tmp = self.last
            self.last = node
            tmp.next = self.last
            self.last.prev = tmp
        self.size+=1
        return True

        

    def deleteFront(self) -> bool:
        if self.isEmpty():
            return False
        if self.size ==1:
            self.front = None
            self.last = None
        else:
            tmp = self.front
            self.front = tmp.next
            self.front.prev = None
            tmp.next = None
        self.size -= 1
        return True
        

    def deleteLast(self) -> bool:
        if self.isEmpty():
            return False
        if self.size ==1:
            self.front = None
            self.last = None
        else:
            tmp = self.last
            self.last = self.last.prev
            self.last.next = None
            tmp.prev = None
        self.size -= 1
        return True
        

    def getFront(self) -> int:
        if self.isEmpty():
            return -1
        return self.front.val
        

    def getRear(self) -> int:
        if self.isEmpty():
            return -1
        return self.last.val
        

    def isEmpty(self) -> bool:
        return self.size == 0
        

    def isFull(self) -> bool:
        return self.size == self.max_size
        


# Your MyCircularDeque object will be instantiated and called as such:
# obj = MyCircularDeque(k)
# param_1 = obj.insertFront(value)
# param_2 = obj.insertLast(value)
# param_3 = obj.deleteFront()
# param_4 = obj.deleteLast()
# param_5 = obj.getFront()
# param_6 = obj.getRear()
# param_7 = obj.isEmpty()
# param_8 = obj.isFull()