class Stack:
    def __init__(self):
        self.values = []
        self.size = 0

    def push(self, value):
        self.values.append(value)
        self.size +=1

    def pop(self):
        if self.isEmpty():
            raise Exception('Stack is empty')
        else:
            val = self.values.pop()
            self.size -= 1
            return val

    def isEmpty(self):
        return self.size == 0
    
    def get(self):
        if self.isEmpty():
            raise Exception('Stack is empty')
        else:
            return self.values[-1]



class Solution:
    def nextGreaterElements(self, nums: List[int]) -> List[int]:
        stack = Stack()
        map_el = {}
        res = [-1]*len(nums)

        for i in range(len(nums)*2):
            index = i % len(nums)
            while not stack.isEmpty() and nums[stack.get()] < nums[index]:
                res[stack.pop()] = nums[index]
            stack.push(index)
        return res