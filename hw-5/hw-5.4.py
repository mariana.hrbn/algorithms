class Node:
    def __init__(self, val):
        self.val = val
        self.next = None

class Queue:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0

    def dequeu(self):
        if self.size == 0:
            return None
        tmp = self.head
        if self.size == 1:
            self.tail = None
            self.head = None
            self.size -=1
        else:
            self.head = tmp.next
            tmp.next =  None
            self.size -=1
        return tmp.val
    
    def enqueu(self, val):
        node = Node(val)
        if self.size == 0:
            self.head = node
            self.tail = node
            self.size = 1
        else:
            tmp = self.tail
            self.tail = node
            tmp.next = self.tail
            self.size += 1
            
        
    
    def top(self):
        if self.size != 0:
            return self.head.val
        else:
            return None

    def length(self):
        return self.size
    

class RecentCounter:

    def __init__(self):
        self.queue = Queue()
        

    def ping(self, t: int) -> int:
        while self.queue.length() != 0 and t - self.queue.top() > 3000:
            self.queue.dequeu()
        self.queue.enqueu(t)
        return self.queue.length()
