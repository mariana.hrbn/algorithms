class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        ans = []
        def backtracking(left,right, prefix):
            if len(prefix) == n*2:
                print(''.join(prefix))
                ans.append(''.join(prefix))
                return
            if left<n:
                backtracking(left+1,right, prefix + ['('] ) 
            if right < left:
                backtracking(left,right+1, prefix + [')'] ) 
        backtracking(0,0,[])
        return ans
            
                
        