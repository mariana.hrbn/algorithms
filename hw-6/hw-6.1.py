class Node:
    def __init__(self, key, value):
        self.value = value
        self.key = key
        self.next = None
        

class LinkedList:
    def __init__(self ):
        self.head = None
        self.length = 0
        
    def prepend(self, key, value):
        new_node = Node(key, value)
        if self.length == 0:
            self.head = new_node
        else:
            new_node.next = self.head
            self.head = new_node
        self.length += 1
        return True

    def get(self, key):
        temp = self.head
        while temp is not None:
            if temp.key == key:
                return temp.value
            temp = temp.next
        return -1
        
    def put(self, key, value):
        if self.length != 0:
            temp = self.head
            while temp is not None:
                if temp.key == key:
                    temp.value = value
                    return True
                else:
                    temp = temp.next
        self.prepend(key, value) 

    def remove(self, key):
        if self.length == 0:
            return False
        if self.head.key == key:
            self.length -=1
            self.head = self.head.next
            return True
        else:
            prev = self.head
            temp = self.head.next
            while temp is not None:
                if temp.key == key:
                    prev.next = temp.next
                    temp.next = None
                    self.length -=1
                    return True
        return False


class MyHashMap:

    def __init__(self, size = 100):
        self.size = size
        self.buckets = [LinkedList()] * self.size


    def put(self, key: int, value: int) -> None:
        hashed_key = self._hash(key)
        self.buckets[hashed_key].put(key, value)
        

    def get(self, key: int) -> int:
        hashed_key = self._hash(key)
        return self.buckets[hashed_key].get(key)
 
    
    def remove(self, key: int) -> None:
        hashed_key = self._hash(key)
        self.buckets[hashed_key].remove(key)


    def _hash(self, key):
        return key % self.size