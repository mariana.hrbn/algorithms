import random


class RandomizedSet():
    def __init__(self):
        self.map = {}
        self.array = []

        
    def insert(self, val: int) -> bool:
        if val in self.map:
            return False
        else:
            self.map[val] = len(self.array)
            self.array.append(val)
            return True
        

    def remove(self, val: int) -> bool:
        if val in self.map:
            last_element, idx = self.array[-1], self.map[val]
            self.array[idx], self.map[last_element] = last_element, idx
            self.array.pop()
            del self.map[val]
            return True
        else:
            return False

    def getRandom(self) -> int:
        random_el =  random.choice(self.array)
        return random_el