class Solution:
    def combine(self, n: int, k: int) -> List[List[int]]:
        def backtracking(prefix, position):
            if len(prefix) == k:
                ans.append(prefix[:])
                return 
            for item in range(position, n+1):
                if not item in prefix:
                    prefix.append(item)
                    backtracking(prefix, item+1)
                    prefix.pop()

        ans = []
        backtracking([], 1)
        return ans